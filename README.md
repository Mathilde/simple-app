# simple-app : Imen AMMAR, Mathilde PREVOST & Mohamed Salah ben Taarit 

# Commandes : 
"ng serve" dans le dossier puis http://localhost:4200/ dans le navigateur
"json-server --watch db.json" puis http://localhost:3000/articles pour voir les articles de la base de données (fichier json)

# Fonctionnalités
- Menu navigation
- Page home
- Page "Article list" qui permet de voir tous les articles
- Champs de recherche pour afficher les articles qui corrrespondent à l'article cherché (title) (sensible à la case)
- Page "New article" pour ajouter un nouvel article
- Lorsque l'on clique sur le titre de l'article, affiche une page avec uniquement cet article
- Sur la page de l'article, bouton "delete" sur chaque article pour pouvoir le supprimer
- Sur la page de l'article, bouton "update" sur chaque article pour pouvoir le modifier (uniquement le content de l'article)

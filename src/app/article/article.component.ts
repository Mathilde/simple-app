///<reference path="../../../node_modules/@angular/core/src/metadata/directives.d.ts"/>
import {Component, Input, Output, OnInit} from '@angular/core';
import {Article} from '../models/article';
import {ArticleService} from '../services/article.service';
import {Observable} from 'rxjs/Observable';
import {ActivatedRoute, Router} from '@angular/router';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {

  @Input()
  article: Article;


  @Output()
  createdArticle: EventEmitter<Article> = new EventEmitter();

  @Output()
  updatedArticle: EventEmitter<Article> = new EventEmitter();


  constructor(private route: ActivatedRoute, private articleService: ArticleService, private router: Router) { }

    ngOnInit() {
        this.route.params.subscribe( params => {
          if (params && params['id']) {
            this.articleService.get(params['id']).subscribe(fetchedArticle => this.article = fetchedArticle);
          }
        });
      }

    delete(){
      this.articleService.delete(this.article.id).subscribe(() => {
        this.router.navigate(['/articles'])});
    }


   create(){
    this.createdArticle.emit(this.article);
    }

    update() {
        this.router.navigate(['/update/' + this.article.id]);
    }

}

import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ArticleService} from '../services/article.service';
import {Article} from '../models/article';
import {RawArticle} from '../models/raw-article';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-article-modif',
  templateUrl: './article-modif.component.html',
  styleUrls: ['./article-modif.component.css']
})
export class ArticleModifComponent implements OnInit {

  articleForm: FormGroup;
  @Input()
  article: Article;

  constructor(private route: ActivatedRoute, private fb: FormBuilder , private articleService: ArticleService, private router : Router) {
    this.articleForm = this.fb.group({
      content : ['', Validators.required ]
    });
  }

  ngOnInit() {
    this.route.params.subscribe( params => {
      if (params && params['id']) {
        this.articleService.get(params['id']).subscribe(fetchedArticle => this.article = fetchedArticle);
      }
    });
  }

  updateArticle() {
    const formModel = this.articleForm.value;
    const myarticle: Article = {
      id : this.article.id,
      title : this.article.title,
      content : formModel.content,
      authors : this.article.authors
    }
    this.articleService.update(myarticle).subscribe((a)=>
      this.router.navigate(['/articles']));
  }

}

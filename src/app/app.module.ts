import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';

import { ArticleService} from './services/article.service';
import { AppComponent } from './app.component';
import { ArticleComponent } from './article/article.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleCreationComponent } from './article-creation/article-creation.component';
import { HomeComponent } from './home/home.component';
import { ArticleModifComponent } from './article-modif/article-modif.component';


  const appRoutes: Routes = [
    { path: 'create', component: ArticleCreationComponent },
    { path: 'articles', component: ArticlesComponent },
    { path: 'article/:id', component: ArticleComponent },
    { path: '', component: HomeComponent },
    { path: 'update/:id', component: ArticleModifComponent},

  ]


@NgModule({
  declarations: [
    AppComponent,
    ArticleComponent,
    ArticlesComponent,
    ArticleCreationComponent,
    HomeComponent,
    ArticleModifComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],

  providers: [ArticleService],
  bootstrap: [AppComponent]






}




)
export class AppModule { }

import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from "../models/article";
import {ArticleService} from "../services/article.service";
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/reduce';
import {nextTick} from 'q';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})


export class ArticlesComponent implements OnInit {


  @Output()
  deletedArticle: EventEmitter<Article> = new EventEmitter();

  private _articles: Article[];

  constructor(private articleService: ArticleService) {
  }

  articles(): Article[] {
    return this._articles;
  }

  create() {
    this.articleService.getAll().subscribe((articles) => this._articles = articles);
  }

  ngOnInit() {
    this.articleService.getAll().subscribe((articles) => this._articles = articles);
  }

  search(event: any) {
    this.articleService.getAll().subscribe((articles) => {
      this._articles = articles.filter(art => art.title.indexOf(event.target.value) !== -1);
    });
  }

}
